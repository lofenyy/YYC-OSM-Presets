errorChk()
{
	if [ "$?" != 0 ]
	then
		echo "Fatal error. Please submit an issue request containing the text above."
		exit
	fi
}

if ! command -v convert &> /dev/null
then
	echo "WARNING: convert, which is part of ImageMagick, could not be found."
fi

if ! command -v inkscape &> /dev/null
then
	echo "ERROR: inkscape could not be found."
	exit
fi

if ! command -v gs &> /dev/null
then
	echo "WARNING: ghostscript could not be found."
fi

mkdir Build/
cd Build/

cp ../Presets/* .

mkdir Assets/
inkscape --export-filename="Assets/Bus Sign Active.png" "../Assets/Bus Sign Active.svg"
errorChk
inkscape --export-filename="Assets/Bus Sign Future.png" "../Assets/Bus Sign Future.svg"
errorChk
inkscape --export-filename="Assets/Bus Sign Zone.png" "../Assets/Bus Sign Zone.svg"
errorChk

echo "Build process appears successful. Archiving..."

zip -r ../YYC-OSM-Presets.zip .
