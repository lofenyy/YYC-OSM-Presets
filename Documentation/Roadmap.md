# 1.0.0 - Everything YYC

**Transit**

* Bus stops (active,inactive,future,zone)
* Bus stations - amenities
* Bus routes
* Bus schedules
* Train stops
* Train stations - amenities
* Train routes
* Train schedules
* Transit accessibility services
* Acknowledge all relevant transit info
* Icons for everything

*All public infra should have presets*

# 0.1.0 - Everything Transit

# 0.0.1 - All bus stop signs

* Bus stops (active,inactive,future,zone)
