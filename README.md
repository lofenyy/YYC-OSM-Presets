# YYC-OSM-Presets

Makes high quality OSM mapping in Calgary much easier. 

With the Calgary Transit Wikiproject underway on OpenStreetMap, it's important to have a way of making it easy to thoroughly map out important features. This need can be easily met with a set of high quality presets designed specifically for this city. 

**Limitations**

Currently designed for use with JOSM and Vespucci, but if you'd like preset support for your map editor, please create an issue ticket. This preset should work with any editor that supports JOSM and beautified JOSM formats. 

## Copyright

Athlough the provided software is licensed under the GNU GPL 3, all image files are licensed under the Creative Commons Attribution Share-alike 4.0 license.

## Recommendations

This preset isn't works to make OpenStreetMap editing in Calgary as easy and high quality as possible. As such, it isn't the only preset you should have installed. Consider the following recommendations. 

## Installation

**Vespucci**

Click the gear icon and select "presets". Click "add presets", then click the SD card icon. Select the archive and click "Ok". To activate, ensure the newly created entry is checked. 

**JOSM**

To install into JOSM, first extract the archive. Then, while in JOSM, click "presets" > "preset preferences" in the menu bar. Add the .xml file to your active presets. Set the root directory of this preset as an icon path. Restart JOSM, and you're ready to go. 

## Building

This step isn't necessary. Currently, it's only possible to build this preset on Unix-like systems. To do so, run the provided sh script. Note that Inkscape must be installed. Ghostscript and Imagemagick are other possible dependencies. 

## Support

The Wiki is available [here.](https://gitea.com/lofenyy/YYC-OSM-Presets/wiki) If these presets are not working as indended, please create an [issue report.](https://gitea.com/lofenyy/YYC-OSM-Presets/issues)

## How To Contribute

Contributions are very welcome. You can help by submitting suggestions and bug reports to the issues section. Writing missing documentation can be very helpful. I'm open to merge requests, and I have no specific requirements. 

